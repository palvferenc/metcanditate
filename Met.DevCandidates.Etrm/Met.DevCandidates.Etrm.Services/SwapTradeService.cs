﻿using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.TradeEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Met.DevCandidates.Etrm.Services
{
    public class SwapTradeService : ISwapTradeService
    {
        StorageContext _context;

        public SwapTradeService(StorageContext context)
        {
            _context = context;
        }

        public List<SwapTrade> GetAllSwapTrades()
        {
            return _context.SwapTrades
                .Include(s => s.Book)
                .Include(s => s.Commodity)
                .Include(s => s.Counterparty)
                .Include(s => s.Trader)
                .ToList();
        }

        public SwapTrade GetSwapTrade(int id)
        {
            return _context.SwapTrades
                .Include(s => s.Book)
                .Include(s => s.Commodity)
                .Include(s => s.Counterparty)
                .Include(s => s.Trader).FirstOrDefault(s => s.Id == id);
        }

        public SwapTrade AddSwapTrade(SwapTrade SwapTrade)
        {
            if(null == _context.Users.Find(SwapTrade.TraderID))
            {
                return null;
            }

            if (null == _context.LegalEntities.Find(SwapTrade.CounterpartyID))
            {
                return null;
            }

            if (null == _context.Books.Find(SwapTrade.BookID))
            {
                return null;
            }

            if (null == _context.Commodities.Find(SwapTrade.CommodityID))
            {
                return null;
            }

            _context.SwapTrades.Add(SwapTrade);
            _context.SaveChanges();
            
            return SwapTrade;
        }

        public SwapTrade UpdateSwapTrade(SwapTrade toUpdate)
        {
            var trade = _context.SwapTrades.Find(toUpdate.Id);

            if (null != trade)
            {
                if (null == _context.Users.Find(toUpdate.TraderID))
                {
                    return null;
                }

                if (null == _context.LegalEntities.Find(toUpdate.CounterpartyID))
                {
                    return null;
                }

                if (null == _context.Books.Find(toUpdate.BookID))
                {
                    return null;
                }

                if (null == _context.Commodities.Find(toUpdate.CommodityID))
                {
                    return null;
                }

                trade.BookID = toUpdate.BookID;
                trade.CounterpartyID = toUpdate.CounterpartyID;
                trade.TraderID = toUpdate.TraderID;
                trade.CommodityID = toUpdate.CommodityID;
                trade.PricingStart = toUpdate.PricingStart;
                trade.PricingEnd = toUpdate.PricingEnd;
                trade.Direction = toUpdate.Direction;
                trade.TradeDate = toUpdate.TradeDate;
                trade.Volume = toUpdate.Volume;
                trade.Price = toUpdate.Price;

                _context.SwapTrades.Update(trade);
                _context.SaveChanges();
                return toUpdate;
            }
            else
            {
                return null;
            }
        }

        public bool DeleteSwapTrade(int id)
        {
            var SwapTrade = _context.SwapTrades.Find(id);

            if (SwapTrade != null)
            {
                _context.SwapTrades.Remove(SwapTrade);
                _context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

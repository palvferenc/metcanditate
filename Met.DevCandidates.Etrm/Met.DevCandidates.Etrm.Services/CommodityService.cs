﻿using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Met.DevCandidates.Etrm.Services
{
    public class CommodityService : ICommodityService
    {
        StorageContext _context;

        public CommodityService(StorageContext context)
        {
            _context = context;
        }

        public List<Commodity> GetAllCommoditys()
        {
            return _context.Commodities.ToList();
        }

        public Commodity GetCommodity(int id)
        {
            return _context.Commodities.Find(id);
        }

        public Commodity AddCommodity(Commodity Commodity)
        {
            _context.Commodities.Add(Commodity);
            _context.SaveChanges();

            return Commodity;
        }

        public Commodity UpdateCommodity(Commodity toUpdate)
        {
            var commodity = _context.Commodities.Find(toUpdate.Id);

            if (null != commodity)
            {
                commodity.Name = toUpdate.Name;
                _context.Commodities.Update(commodity);
                _context.SaveChanges();
                return toUpdate;
            }
            else
            {
                return null;
            }
        }

        public bool DeleteCommodity(int id)
        {
            var Commodity = _context.Commodities.Find(id);

            if (Commodity != null)
            {
                _context.Commodities.Remove(Commodity);
                _context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

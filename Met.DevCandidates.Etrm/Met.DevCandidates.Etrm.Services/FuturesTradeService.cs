﻿using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.TradeEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Met.DevCandidates.Etrm.Services
{
    public class FuturesTradeService : IFuturesTradeService
    {
        StorageContext _context;

        public FuturesTradeService(StorageContext context)
        {
            _context = context;
        }

        public List<FuturesTrade> GetAllFuturesTrades()
        {
            return _context.FuturesTrades
                .Include(s => s.Book)
                .Include(s => s.Commodity)
                .Include(s => s.Counterparty)
                .Include(s => s.Trader).ToList();
        }

        public FuturesTrade GetFuturesTrade(int id)
        {
            return _context.FuturesTrades
                .Include(s => s.Book)
                .Include(s => s.Commodity)
                .Include(s => s.Counterparty)
                .Include(s => s.Trader).FirstOrDefault(s => s.Id == id);
        }

        public FuturesTrade AddFuturesTrade(FuturesTrade FuturesTrade)
        {
            if (null == _context.Users.Find(FuturesTrade.TraderID))
            {
                return null;
            }

            if (null == _context.LegalEntities.Find(FuturesTrade.CounterpartyID))
            {
                return null;
            }

            if (null == _context.Books.Find(FuturesTrade.BookID))
            {
                return null;
            }

            if (null == _context.Commodities.Find(FuturesTrade.CommodityID))
            {
                return null;
            }
            
            _context.FuturesTrades.Add(FuturesTrade);
            _context.SaveChanges();

            return FuturesTrade;
        }

        public FuturesTrade UpdateFuturesTrade(FuturesTrade toUpdate)
        {
            var trade = _context.FuturesTrades.Find(toUpdate.Id);

            if (null != trade)
            {
                if (null == _context.Users.Find(toUpdate.TraderID))
                {
                    return null;
                }

                if (null == _context.LegalEntities.Find(toUpdate.CounterpartyID))
                {
                    return null;
                }

                if (null == _context.Books.Find(toUpdate.BookID))
                {
                    return null;
                }

                if (null == _context.Commodities.Find(toUpdate.CommodityID))
                {
                    return null;
                }

                trade.BookID = toUpdate.BookID;
                trade.CounterpartyID = toUpdate.CounterpartyID;
                trade.TraderID = toUpdate.TraderID;
                trade.CommodityID = toUpdate.CommodityID;
                trade.DeliveryDate = toUpdate.DeliveryDate;
                trade.Direction = toUpdate.Direction;
                trade.TradeDate = toUpdate.TradeDate;
                trade.Volume = toUpdate.Volume;
                trade.Price = toUpdate.Price;

                _context.FuturesTrades.Update(trade);
                _context.SaveChanges();
                return toUpdate;
            }
            else
            {
                return null;
            }
        }

        public bool DeleteFuturesTrade(int id)
        {
            var FuturesTrade = _context.FuturesTrades.Find(id);

            if (FuturesTrade != null)
            {
                _context.FuturesTrades.Remove(FuturesTrade);
                _context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

﻿using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Met.DevCandidates.Etrm.Services
{
    public class LegalEntityService : ILegalEntityService
    {
        StorageContext _context;

        public LegalEntityService(StorageContext context)
        {
            _context = context;
        }

        public List<LegalEntity> GetAllLegalEntitys()
        {
            return _context.LegalEntities.ToList();
        }

        public LegalEntity GetLegalEntity(int id)
        {
            return _context.LegalEntities.Find(id);
        }

        public LegalEntity AddLegalEntity(LegalEntity LegalEntity)
        {
            _context.LegalEntities.Add(LegalEntity);
            _context.SaveChanges();

            return LegalEntity;
        }

        public LegalEntity UpdateLegalEntity(LegalEntity toUpdate)
        {
            var legalEntity = _context.LegalEntities.Find(toUpdate.Id);
            if (null != legalEntity)
            {
                legalEntity.Name = toUpdate.Name;
                _context.LegalEntities.Update(legalEntity);
                _context.SaveChanges();
                return toUpdate;
            }
            else
            {
                return null;
            }
        }

        public bool DeleteLegalEntity(int id)
        {
            var LegalEntity = _context.LegalEntities.Find(id);

            if (LegalEntity != null)
            {
                _context.LegalEntities.Remove(LegalEntity);
                _context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

﻿using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Met.DevCandidates.Etrm.Services
{
    public class UserService : IUserService
    {
        StorageContext _context;

        public UserService(StorageContext context)
        {
            _context = context;
        }

        public List<User> GetAllUsers()
        {
            return _context.Users.ToList();
        }

        public User GetUser(int id)
        {
            return _context.Users.Find(id);
        }

        public User AddUser(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public User UpdateUser(User toUpdate)
        {
            var user = _context.Users.Find(toUpdate.Id);
            if (null != user)
            {
                user.FirstName = toUpdate.FirstName;
                user.SecondName = toUpdate.SecondName;
                _context.Users.Update(user);
                _context.SaveChanges();
                return toUpdate;
            }
            else
            {
                return null;
            }
        }

        public bool DeleteUser(int id)
        {
            var user = _context.Users.Find(id);

            if(user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

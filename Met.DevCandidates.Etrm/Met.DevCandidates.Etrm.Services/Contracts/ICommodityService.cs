﻿using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Met.DevCandidates.Etrm.Services.Contracts
{
    public interface ICommodityService
    {
        List<Commodity> GetAllCommoditys();
        Commodity GetCommodity(int id);
        Commodity AddCommodity(Commodity commodity);
        Commodity UpdateCommodity(Commodity toUpdate);
        bool DeleteCommodity(int id);
    }
}

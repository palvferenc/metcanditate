﻿using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Met.DevCandidates.Etrm.Services.Contracts
{
    public interface ILegalEntityService
    {
        List<LegalEntity> GetAllLegalEntitys();
        LegalEntity GetLegalEntity(int id);
        LegalEntity AddLegalEntity(LegalEntity legalEntity);
        LegalEntity UpdateLegalEntity(LegalEntity toUpdate);
        bool DeleteLegalEntity(int id);
    }
}

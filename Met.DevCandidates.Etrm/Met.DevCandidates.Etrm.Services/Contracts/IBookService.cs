﻿using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Met.DevCandidates.Etrm.Services.Contracts
{
    public interface IBookService
    {
        List<Book> GetAllBooks();
        Book GetBook(int id);
        Book AddBook(Book book);
        Book UpdateBook(Book toUpdate);
        bool DeleteBook(int id);
    }
}

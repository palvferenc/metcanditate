﻿using Met.DevCandidates.Etrm.Data.TradeEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Met.DevCandidates.Etrm.Services.Contracts
{
    public interface IFuturesTradeService
    {
        List<FuturesTrade> GetAllFuturesTrades();
        FuturesTrade GetFuturesTrade(int id);
        FuturesTrade AddFuturesTrade(FuturesTrade futuresTrade);
        FuturesTrade UpdateFuturesTrade(FuturesTrade toUpdate);
        bool DeleteFuturesTrade(int id);
    }
}

﻿using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Met.DevCandidates.Etrm.Services.Contracts
{
    public interface IUserService
    {
        List<User> GetAllUsers();
        User GetUser(int id);
        User AddUser(User user);
        User UpdateUser(User toUpdate);
        bool DeleteUser(int id);
    }
}

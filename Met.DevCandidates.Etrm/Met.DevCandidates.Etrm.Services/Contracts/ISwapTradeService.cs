﻿using Met.DevCandidates.Etrm.Data.TradeEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Met.DevCandidates.Etrm.Services.Contracts
{
    public interface ISwapTradeService
    {
        List<SwapTrade> GetAllSwapTrades();
        SwapTrade GetSwapTrade(int id);
        SwapTrade AddSwapTrade(SwapTrade swapTrade);
        SwapTrade UpdateSwapTrade(SwapTrade toUpdate);
        bool DeleteSwapTrade(int id);
    }
}

﻿using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Met.DevCandidates.Etrm.Services
{
    public class BookService : IBookService
    {
        StorageContext _context;

        public BookService(StorageContext context)
        {
            _context = context;
        }

        public List<Book> GetAllBooks()
        {
            return _context.Books.ToList();
        }

        public Book GetBook(int id)
        {
            return _context.Books.Find(id);
        }

        public Book AddBook(Book Book)
        {
            _context.Books.Add(Book);
            _context.SaveChanges();

            return Book;
        }

        public Book UpdateBook(Book toUpdate)
        {
            var book = _context.Books.Find(toUpdate.Id);
            if (null != book)
            {
                book.Name = toUpdate.Name;
                _context.Books.Update(book);
                _context.SaveChanges();
                return toUpdate;
            }
            else
            {
                return null;
            }
        }

        public bool DeleteBook(int id)
        {
            var Book = _context.Books.Find(id);

            if (Book != null)
            {
                _context.Books.Remove(Book);
                _context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

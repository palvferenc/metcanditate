using FluentAssert;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System.Linq;
using Microsoft.Data.Sqlite;

namespace Met.DevCandidates.Etrm.Services.Test
{
    public class UserServiceTest
    {
        [Fact]
        public void Test_GetAllUsers()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Users.Add(new User() { FirstName = "John", SecondName = "Pocket" });
                    storage.Users.Add(new User() { FirstName = "Ian", SecondName = "McLachlan" });
                    storage.Users.Add(new User() { FirstName = "Rose", SecondName = "Gidion" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    UserService underTest = new UserService(storage);
                    var result = underTest.GetAllUsers();

                    result.ShouldNotBeNull();

                    result.Count.ShouldBeEqualTo(3);

                    result[0].FirstName.ShouldBeEqualTo("John");
                    result[0].SecondName.ShouldBeEqualTo("Pocket");

                    result[2].FirstName.ShouldBeEqualTo("Rose");
                    result[2].SecondName.ShouldBeEqualTo("Gidion");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_AddUser()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Users.Add(new User() { FirstName = "John", SecondName = "Pocket" });
                    storage.Users.Add(new User() { FirstName = "Ian", SecondName = "McLachlan" });
                    storage.Users.Add(new User() { FirstName = "Rose", SecondName = "Gidion" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IUserService underTest = new UserService(storage);

                    User added = underTest.AddUser(new User { FirstName = "Carol", SecondName = "Grifin" });

                    added.Id.ShouldBeEqualTo(4);
                    storage.Users.Where(u => u.FirstName == "Carol").Single().FirstName.ShouldBeEqualTo("Carol");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_GetUser()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Users.Add(new User() { FirstName = "John", SecondName = "Pocket" });
                    storage.Users.Add(new User() { FirstName = "Ian", SecondName = "McLachlan" });
                    storage.Users.Add(new User() { FirstName = "Rose", SecondName = "Gidion" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IUserService underTest = new UserService(storage);

                    User result = underTest.GetUser(2);

                    result.ShouldNotBeNull().FirstName.ShouldBeEqualTo("Ian");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_UpdateUser()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
               .UseSqlite(connection)
               .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Users.Add(new User() { FirstName = "John", SecondName = "Pocket" });
                    storage.Users.Add(new User() { FirstName = "Ian", SecondName = "McLachlan" });
                    storage.Users.Add(new User() { FirstName = "Rose", SecondName = "Gidion" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IUserService underTest = new UserService(storage);

                    User toUpdate = storage.Users.Find(2);

                    toUpdate.FirstName = "Han";
                    User updated = underTest.UpdateUser(toUpdate);

                    updated.ShouldNotBeNull().FirstName.ShouldBeEqualTo("Han");

                    User loaded = storage.Users.Find(2);
                    loaded.ShouldNotBeNull().FirstName.ShouldBeEqualTo("Han");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_DeleteUser()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Users.Add(new User() { FirstName = "John", SecondName = "Pocket" });
                    storage.Users.Add(new User() { FirstName = "Ian", SecondName = "McLachlan" });
                    storage.Users.Add(new User() { FirstName = "Rose", SecondName = "Gidion" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IUserService underTest = new UserService(storage);

                    bool success = underTest.DeleteUser(3);

                    success.ShouldBeEqualTo(true);

                    success = underTest.DeleteUser(123);

                    success.ShouldBeEqualTo(false);
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

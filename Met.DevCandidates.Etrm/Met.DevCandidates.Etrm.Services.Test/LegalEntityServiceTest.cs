using FluentAssert;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System.Linq;
using Microsoft.Data.Sqlite;

namespace Met.DevCandidates.Etrm.Services.Test
{
    public class LegalEntitieserviceTest
    {
        [Fact]
        public void Test_GetAllLegalEntities()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John"});
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ILegalEntityService underTest = new LegalEntityService(storage);
                    var result = underTest.GetAllLegalEntitys();

                    result.ShouldNotBeNull();

                    result.Count.ShouldBeEqualTo(3);

                    result[0].Name.ShouldBeEqualTo("John");

                    result[2].Name.ShouldBeEqualTo("Rose");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_AddLegalEntity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ILegalEntityService underTest = new LegalEntityService(storage);

                    LegalEntity added = underTest.AddLegalEntity(new LegalEntity { Name = "Carol"});

                    added.Id.ShouldBeEqualTo(4);
                    storage.LegalEntities.Where(u => u.Name == "Carol").Single().Name.ShouldBeEqualTo("Carol");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_GetLegalEntity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ILegalEntityService underTest = new LegalEntityService(storage);

                    LegalEntity result = underTest.GetLegalEntity(2);

                    result.ShouldNotBeNull().Name.ShouldBeEqualTo("Ian");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_UpdateLegalEntity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
               .UseSqlite(connection)
               .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ILegalEntityService underTest = new LegalEntityService(storage);

                    LegalEntity toUpdate = storage.LegalEntities.Find(2);

                    toUpdate.Name = "Han";
                    LegalEntity updated = underTest.UpdateLegalEntity(toUpdate);

                    updated.ShouldNotBeNull().Name.ShouldBeEqualTo("Han");

                    LegalEntity loaded = storage.LegalEntities.Find(2);
                    loaded.ShouldNotBeNull().Name.ShouldBeEqualTo("Han");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_DeleteLegalEntity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });
                    storage.LegalEntities.Add(new LegalEntity() { Name = "John" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ILegalEntityService underTest = new LegalEntityService(storage);

                    bool success = underTest.DeleteLegalEntity(3);

                    success.ShouldBeEqualTo(true);

                    success = underTest.DeleteLegalEntity(123);

                    success.ShouldBeEqualTo(false);
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

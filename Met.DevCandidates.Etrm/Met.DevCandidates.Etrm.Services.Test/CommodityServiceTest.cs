using FluentAssert;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System.Linq;
using Microsoft.Data.Sqlite;

namespace Met.DevCandidates.Etrm.Services.Test
{
    public class CommodityServiceTest
    {
        [Fact]
        public void Test_GetAllCommoditys()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Commodities.Add(new Commodity() { Name = "John"});
                    storage.Commodities.Add(new Commodity() { Name = "Ian" });
                    storage.Commodities.Add(new Commodity() { Name = "Rose"});

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    CommodityService underTest = new CommodityService(storage);
                    var result = underTest.GetAllCommoditys();

                    result.ShouldNotBeNull();

                    result.Count.ShouldBeEqualTo(3);

                    result[0].Name.ShouldBeEqualTo("John");

                    result[2].Name.ShouldBeEqualTo("Rose");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_AddCommodity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Commodities.Add(new Commodity() { Name = "John"});
                    storage.Commodities.Add(new Commodity() { Name = "Ian" });
                    storage.Commodities.Add(new Commodity() { Name = "Rose"});

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ICommodityService underTest = new CommodityService(storage);

                    Commodity added = underTest.AddCommodity(new Commodity { Name = "Carol"});

                    added.Id.ShouldBeEqualTo(4);
                    storage.Commodities.Where(u => u.Name == "Carol").Single().Name.ShouldBeEqualTo("Carol");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_GetCommodity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Commodities.Add(new Commodity() { Name = "John"});
                    storage.Commodities.Add(new Commodity() { Name = "Ian"});
                    storage.Commodities.Add(new Commodity() { Name = "Rose"});

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ICommodityService underTest = new CommodityService(storage);

                    Commodity result = underTest.GetCommodity(2);

                    result.ShouldNotBeNull().Name.ShouldBeEqualTo("Ian");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_UpdateCommodity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
               .UseSqlite(connection)
               .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Commodities.Add(new Commodity() { Name = "John"});
                    storage.Commodities.Add(new Commodity() { Name = "Ian"});
                    storage.Commodities.Add(new Commodity() { Name = "Rose"});

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ICommodityService underTest = new CommodityService(storage);

                    Commodity toUpdate = storage.Commodities.Find(2);

                    toUpdate.Name = "Han";
                    Commodity updated = underTest.UpdateCommodity(toUpdate);

                    updated.ShouldNotBeNull().Name.ShouldBeEqualTo("Han");

                    Commodity loaded = storage.Commodities.Find(2);
                    loaded.ShouldNotBeNull().Name.ShouldBeEqualTo("Han");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_DeleteCommodity()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Commodities.Add(new Commodity() { Name = "John"});
                    storage.Commodities.Add(new Commodity() { Name = "Ian"});
                    storage.Commodities.Add(new Commodity() { Name = "Rose"});

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ICommodityService underTest = new CommodityService(storage);

                    bool success = underTest.DeleteCommodity(3);

                    success.ShouldBeEqualTo(true);

                    success = underTest.DeleteCommodity(123);

                    success.ShouldBeEqualTo(false);
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

using FluentAssert;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System.Linq;
using Microsoft.Data.Sqlite;
using Met.DevCandidates.Etrm.Data.TradeEntities;

namespace Met.DevCandidates.Etrm.Services.Test
{
    public class SwapTradeServiceTest
    {
        [Fact]
        public void Test_GetAllSwapTrades()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    SwapTradeService underTest = new SwapTradeService(storage);
                    var result = underTest.GetAllSwapTrades();

                    result.ShouldNotBeNull();

                    result.Count.ShouldBeEqualTo(3);

                    result[0].Volume.ShouldBeEqualTo(1);

                    result[2].Volume.ShouldBeEqualTo(1);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_AddSwapTrade()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ISwapTradeService underTest = new SwapTradeService(storage);

                    SwapTrade added = underTest.AddSwapTrade(new SwapTrade { Volume = 1});

                    added.Id.ShouldBeEqualTo(4);
                    storage.SwapTrades.Where(s => s.Volume == 1).Single().Volume.ShouldBeEqualTo(1);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_GetSwapTrade()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ISwapTradeService underTest = new SwapTradeService(storage);

                    SwapTrade result = underTest.GetSwapTrade(2);

                    result.ShouldNotBeNull().Volume.ShouldBeEqualTo(1);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_UpdateSwapTrade()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
               .UseSqlite(connection)
               .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ISwapTradeService underTest = new SwapTradeService(storage);

                    SwapTrade toUpdate = storage.SwapTrades.Find(2);

                    toUpdate.Volume = 1;
                    SwapTrade updated = underTest.UpdateSwapTrade(toUpdate);

                    updated.ShouldNotBeNull().Volume.ShouldBeEqualTo(1);

                    SwapTrade loaded = storage.SwapTrades.Find(2);
                    loaded.ShouldNotBeNull().Volume.ShouldBeEqualTo(1);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_DeleteSwapTrade()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });
                    storage.SwapTrades.Add(new SwapTrade() { Volume = 1 });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    ISwapTradeService underTest = new SwapTradeService(storage);

                    bool success = underTest.DeleteSwapTrade(3);

                    success.ShouldBeEqualTo(true);

                    success = underTest.DeleteSwapTrade(123);

                    success.ShouldBeEqualTo(false);
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

using FluentAssert;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System.Linq;
using Microsoft.Data.Sqlite;

namespace Met.DevCandidates.Etrm.Services.Test
{
    public class BookServiceTest
    {
        [Fact]
        public void Test_GetAllBooks()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Books.Add(new Book() { Name = "John"});
                    storage.Books.Add(new Book() { Name = "Ian" });
                    storage.Books.Add(new Book() { Name = "Rose" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    BookService underTest = new BookService(storage);
                    var result = underTest.GetAllBooks();

                    result.ShouldNotBeNull();

                    result.Count.ShouldBeEqualTo(3);

                    result[0].Name.ShouldBeEqualTo("John");

                    result[2].Name.ShouldBeEqualTo("Rose");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_AddBook()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Books.Add(new Book() { Name = "John" });
                    storage.Books.Add(new Book() { Name = "Ian" });
                    storage.Books.Add(new Book() { Name = "Rose" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IBookService underTest = new BookService(storage);

                    Book added = underTest.AddBook(new Book { Name = "Carol"});

                    added.Id.ShouldBeEqualTo(4);
                    storage.Books.Where(u => u.Name == "Carol").Single().Name.ShouldBeEqualTo("Carol");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_GetBook()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Books.Add(new Book() { Name = "John" });
                    storage.Books.Add(new Book() { Name = "Ian" });
                    storage.Books.Add(new Book() { Name = "Rose" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IBookService underTest = new BookService(storage);

                    Book result = underTest.GetBook(2);

                    result.ShouldNotBeNull().Name.ShouldBeEqualTo("Ian");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_UpdateBook()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
               .UseSqlite(connection)
               .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Books.Add(new Book() { Name = "John" });
                    storage.Books.Add(new Book() { Name = "Ian" });
                    storage.Books.Add(new Book() { Name = "Rose" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IBookService underTest = new BookService(storage);

                    Book toUpdate = storage.Books.Find(2);

                    toUpdate.Name = "Han";
                    Book updated = underTest.UpdateBook(toUpdate);

                    updated.ShouldNotBeNull().Name.ShouldBeEqualTo("Han");

                    Book loaded = storage.Books.Find(2);
                    loaded.ShouldNotBeNull().Name.ShouldBeEqualTo("Han");
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_DeleteBook()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Books.Add(new Book() { Name = "John"});
                    storage.Books.Add(new Book() { Name = "Ian" });
                    storage.Books.Add(new Book() { Name = "Rose" });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IBookService underTest = new BookService(storage);

                    bool success = underTest.DeleteBook(3);

                    success.ShouldBeEqualTo(true);

                    success = underTest.DeleteBook(123);

                    success.ShouldBeEqualTo(false);
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

using FluentAssert;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using System.Linq;
using Microsoft.Data.Sqlite;
using Met.DevCandidates.Etrm.Data.TradeEntities;
using System;

namespace Met.DevCandidates.Etrm.Services.Test
{
    public class FuturesTradeerviceTest
    {
        [Fact]
        public void Test_GetAllFuturesTrade()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now});
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    FuturesTradeService underTest = new FuturesTradeService(storage);
                    var result = underTest.GetAllFuturesTrades();

                    result.ShouldNotBeNull();

                    result.Count.ShouldBeEqualTo(3);

                    result[0].DeliveryDate.ShouldBeEqualTo(DateTime.Now);

                    result[2].DeliveryDate.ShouldBeEqualTo(DateTime.Now);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_AddFutures()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IFuturesTradeService underTest = new FuturesTradeService(storage);

                    FuturesTrade added = underTest.AddFuturesTrade(new FuturesTrade { DeliveryDate = DateTime.Now });

                    added.Id.ShouldBeEqualTo(4);
                    storage.FuturesTrades.Where(ft => ft.DeliveryDate.Equals(DateTime.Now)).Single().DeliveryDate.ShouldBeEqualTo(DateTime.Now);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_GetFutures()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IFuturesTradeService underTest = new FuturesTradeService(storage);

                    FuturesTrade result = underTest.GetFuturesTrade(2);

                    result.ShouldNotBeNull().DeliveryDate.ShouldBeEqualTo(DateTime.Now);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_UpdateFutures()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
               .UseSqlite(connection)
               .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IFuturesTradeService underTest = new FuturesTradeService(storage);

                    FuturesTrade toUpdate = storage.FuturesTrades.Find(2);

                    toUpdate.DeliveryDate = DateTime.Now;
                    FuturesTrade updated = underTest.UpdateFuturesTrade(toUpdate);

                    updated.ShouldNotBeNull().DeliveryDate.ShouldBeEqualTo(DateTime.Now);

                    FuturesTrade loaded = storage.FuturesTrades.Find(2);
                    loaded.ShouldNotBeNull().DeliveryDate.ShouldBeEqualTo(DateTime.Now);
                }
            }
            catch
            {
                connection.Close();
            }
        }

        [Fact]
        public void Test_DeleteFutures()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var storageOptions = new DbContextOptionsBuilder<StorageContext>()
                .UseSqlite(connection)
                .Options;

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.Database.EnsureCreated();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });
                    storage.FuturesTrades.Add(new FuturesTrade() { DeliveryDate = DateTime.Now });

                    storage.SaveChanges();
                }

                using (var storage = new StorageContext(storageOptions))
                {
                    IFuturesTradeService underTest = new FuturesTradeService(storage);

                    bool success = underTest.DeleteFuturesTrade(3);

                    success.ShouldBeEqualTo(true);

                    success = underTest.DeleteFuturesTrade(123);

                    success.ShouldBeEqualTo(false);
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

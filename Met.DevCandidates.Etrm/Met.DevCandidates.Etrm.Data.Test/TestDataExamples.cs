﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Met.DevCandidates.Etrm.Data.Test
{
    [TestClass]
    public class TestDataExamples
    {
        [TestMethod]
        public void TestGetAllFuturesTrades()
        {
            var futuresTrades = DataExamples.GetFuturesTrades();
            Assert.AreEqual(2, futuresTrades.Count);
        }

        [TestMethod]
        public void TestGetAllSwapTrades()
        {
            var swapTrades = DataExamples.GetSwapTrades();
            Assert.AreEqual(1, swapTrades.Count);
        }

        [TestMethod]
        public void TestGetAll()
        {
            var trades = DataExamples.GetAllTrades();
            Assert.AreEqual(3, trades.Count);
        }

    }
}

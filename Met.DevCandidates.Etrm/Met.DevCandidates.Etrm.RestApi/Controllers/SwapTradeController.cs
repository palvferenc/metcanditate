﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Data.TradeEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Met.DevCandidates.Etrm.RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/swaptrades")]
    public class SwapTradeController : Controller
    {
        private ISwapTradeService _swapTradeService;

        public SwapTradeController(ISwapTradeService swapTradeService)
        {
            _swapTradeService = swapTradeService;
        }

        [HttpGet]
        public IEnumerable<SwapTrade> GetAll()
        {
            return _swapTradeService.GetAllSwapTrades();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            SwapTrade result = _swapTradeService.GetSwapTrade(id);
            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public SwapTrade Post([FromBody]SwapTrade swapTrade)
        {
            return _swapTradeService.AddSwapTrade(swapTrade);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]SwapTrade swapTrade)
        {
            SwapTrade result = _swapTradeService.UpdateSwapTrade(swapTrade);

            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if(!_swapTradeService.DeleteSwapTrade(id))
            {
                return NotFound();
            }
            else
            {
                return Ok();
            }
        }
    }
}
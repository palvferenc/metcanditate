﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Met.DevCandidates.Etrm.RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/books")]
    public class BookController : Controller
    {
        private IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet]
        public IEnumerable<Book> GetAll()
        {
            return _bookService.GetAllBooks();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Book result = _bookService.GetBook(id);
            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public Book Post([FromBody]Book user)
        {
            return _bookService.AddBook(user);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Book user)
        {
            Book result = _bookService.UpdateBook(user);

            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if(!_bookService.DeleteBook(id))
            {
                return NotFound();
            }
            else
            {
                return Ok();
            }
        }
    }
}
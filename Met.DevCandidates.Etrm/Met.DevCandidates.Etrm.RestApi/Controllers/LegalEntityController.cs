﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Met.DevCandidates.Etrm.RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/legalentities")]
    public class LegalEntityController : Controller
    {
        private ILegalEntityService _legalEntityService;

        public LegalEntityController(ILegalEntityService legalEntityService)
        {
            _legalEntityService = legalEntityService;
        }

        [HttpGet]
        public IEnumerable<LegalEntity> GetAll()
        {
            return _legalEntityService.GetAllLegalEntitys();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            LegalEntity result = _legalEntityService.GetLegalEntity(id);
            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public LegalEntity Post([FromBody]LegalEntity legalEntity)
        {
            return _legalEntityService.AddLegalEntity(legalEntity);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]LegalEntity legalEntity)
        {
            LegalEntity result = _legalEntityService.UpdateLegalEntity(legalEntity);

            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if(!_legalEntityService.DeleteLegalEntity(id))
            {
                return NotFound();
            }
            else
            {
                return Ok();
            }
        }
    }
}
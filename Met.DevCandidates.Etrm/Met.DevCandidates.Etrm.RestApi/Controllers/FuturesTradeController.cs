﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Data.TradeEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Met.DevCandidates.Etrm.RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/futurestrades")]
    public class FuturesTradeController : Controller
    {
        private IFuturesTradeService _futuresTradeService;

        public FuturesTradeController(IFuturesTradeService futuresTradeService)
        {
            _futuresTradeService = futuresTradeService;
        }

        [HttpGet]
        public IEnumerable<FuturesTrade> GetAll()
        {
            return _futuresTradeService.GetAllFuturesTrades();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            FuturesTrade result = _futuresTradeService.GetFuturesTrade(id);
            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public FuturesTrade Post([FromBody]FuturesTrade futuresTrade)
        {
            return _futuresTradeService.AddFuturesTrade(futuresTrade);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]FuturesTrade futuresTrade)
        {
            FuturesTrade result = _futuresTradeService.UpdateFuturesTrade(futuresTrade);

            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if(!_futuresTradeService.DeleteFuturesTrade(id))
            {
                return NotFound();
            }
            else
            {
                return Ok();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Met.DevCandidates.Etrm.RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/commodities")]
    public class CommodityController : Controller
    {
        private ICommodityService _commodityService;

        public CommodityController(ICommodityService commodityService)
        {
            _commodityService = commodityService;
        }

        [HttpGet]
        public IEnumerable<Commodity> GetAll()
        {
            return _commodityService.GetAllCommoditys();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Commodity result = _commodityService.GetCommodity(id);
            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public Commodity Post([FromBody]Commodity commodity)
        {
            return _commodityService.AddCommodity(commodity);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Commodity commodity)
        {
            Commodity result = _commodityService.UpdateCommodity(commodity);

            if (null != result)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if(!_commodityService.DeleteCommodity(id))
            {
                return NotFound();
            }
            else
            {
                return Ok();
            }
        }
    }
}
﻿using Met.DevCandidates.Etrm.Data;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Data.TradeEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Met.DevCandidates.Etrm.RestApi
{
    public static class GenerateData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new StorageContext(
                    serviceProvider.GetRequiredService<DbContextOptions<StorageContext>>()
                ))
            {
                if(context.Users.Any())
                {
                    return;
                }

                context.Users.AddRange(
                    new User { FirstName = "Fred", SecondName = "Bloggs" },
                    new User { FirstName = "Stephen", SecondName = "Malley" },
                    new User { FirstName = "John", SecondName = "Smith" },
                    new User { FirstName = "Neil", SecondName = "Barnet" }
                    );

                context.Books.AddRange(
                    new Book { Name = "Physical Book" },
                    new Book { Name = "Paper Book" },
                    new Book { Name = "Foreign Exchange Book" }
                    );

                context.Commodities.AddRange(
                    new Commodity { Name = "Crude Oil" },
                    new Commodity { Name = "Diesel" }
                    );

                context.LegalEntities.AddRange(
                    new LegalEntity { Name = "MOL" },
                    new LegalEntity { Name = "Gazprom" }
                    );

                context.SaveChanges();

                context.FuturesTrades.AddRange(
                        new FuturesTrade
                        {
                            TradeDate = new DateTime(2018, 1, 2),
                            Price = 56.85M,
                            Volume = 1000,
                            DeliveryDate = new DateTime(2018, 12, 1),
                            Direction = Direction.Buy,
                            Trader = context.Users.Single(user => user.FirstName == "John"),
                            Book = context.Books.Single(book => book.Name == "Paper Book"),
                            Commodity = context.Commodities.Single(commodity => commodity.Name == "Crude Oil"),
                            Counterparty = context.LegalEntities.Single(legalEntity => legalEntity.Name == "Gazprom"),
                        },
                        new FuturesTrade
                        {
                            TradeDate = new DateTime(2018, 1, 4),
                            Price = 56.01M,
                            Volume = 5000,
                            DeliveryDate = new DateTime(2018, 12, 1),
                            Direction = Direction.Sell,
                            Trader = context.Users.Single(user => user.FirstName == "Fred"),
                            Book = context.Books.Single(book => book.Name == "Paper Book"),
                            Commodity = context.Commodities.Single(commodity => commodity.Name == "Diesel"),
                            Counterparty = context.LegalEntities.Single(legalEntity => legalEntity.Name == "MOL"),
                        }
                    );

                context.SwapTrades.AddRange(
                    new SwapTrade
                    {
                        TradeDate = new DateTime(2018, 1, 2),
                        Price = 56.85M,
                        Volume = 1000,
                        PricingStart = new DateTime(2019, 1, 1),
                        PricingEnd = new DateTime(2019, 1, 31),
                        Direction = Direction.Buy,
                        Trader = context.Users.Single(user => user.FirstName == "Stephen"),
                        Book = context.Books.Single(book => book.Name == "Paper Book"),
                        Commodity = context.Commodities.Single(commodity => commodity.Name == "Crude Oil"),
                        Counterparty = context.LegalEntities.Single(legalEntity => legalEntity.Name == "Gazprom"),
                    }
                    );

                context.SaveChanges();
            }
        }
    }
}

import { Component, OnInit, Input } from '@angular/core';
import { SwaptradesService } from '../swaptrades.service';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SwapTrade } from '../models/swaptrade';
import { UserService } from '../user.service';
import { BooksService } from '../books.service';
import { LegalentitiesService } from '../legalentities.service';
import { CommiditiesService } from '../commidities.service';
import { User } from '../models/user';
import { Book } from '../models/book';
import { LegalEntity } from '../models/legal-entity';
import { Commodity } from '../models/commodity';

@Component({
  selector: 'app-swaptrade-detail',
  templateUrl: './swaptrade-detail.component.html',
  styleUrls: ['./swaptrade-detail.component.css']
})
export class SwaptradeDetailComponent implements OnInit {

  @Input() swapTrade: SwapTrade;

  users: User[];
  books: Book[];

  entities: LegalEntity[];
  commodities: Commodity[];

  constructor(
    private route: ActivatedRoute,
    private swapTradeService: SwaptradesService,
    private userService: UserService,
    private booksService: BooksService,
    private legalEntityService: LegalentitiesService,
    private commodityService:CommiditiesService,
    private location: Location
  ) {}

  ngOnInit() {
    this.getUsers();
    this.getBooks();
    this.getLegalEntities();
    this.getcCommodities();
    this.getSwapTrade();
  }

  getSwapTrade(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.swapTradeService.getSwapTrade(id)
      .subscribe(swapTrade => this.swapTrade = swapTrade);
  }

  getUsers() : void
  {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  getcCommodities() : void
  {
    this.commodityService.getCommodities().subscribe(commidities => this.commodities = commidities);
  }

  getLegalEntities() : void
  {
    this.legalEntityService.getLegalEntities().subscribe(legalEntities => this.entities = legalEntities);
  }

  getBooks() : void
  {
    this.booksService.getBooks().subscribe(books => this.books = books);
  }

  goBack(): void {
    this.location.back();
  }
 
 save(): void {
  if(
    !this.swapTrade.price 
    || !this.swapTrade.pricingStart 
    || !this.swapTrade.pricingEnd
    || !this.swapTrade.traderId 
    || !this.swapTrade.commodityId
    || !this.swapTrade.counterPartyId
    || !this.swapTrade.bookId
    || !this.swapTrade.volume
    || !this.swapTrade.tradeDate
  ) { return; }
    this.swapTradeService.updateSwapTrade(this.swapTrade)
      .subscribe(() => this.goBack());
  }
}

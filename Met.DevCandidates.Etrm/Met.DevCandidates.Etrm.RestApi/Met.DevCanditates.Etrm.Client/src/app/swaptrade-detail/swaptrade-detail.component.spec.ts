import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwaptradeDetailComponent } from './swaptrade-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { SwaptradesService } from '../swaptrades.service';
import { SwapTradesServiceSpy } from '../../../testing/spies/swaptrades.service.spy';

describe('SwaptradeDetailComponent', () => {
  let component: SwaptradeDetailComponent;
  let fixture: ComponentFixture<SwaptradeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ SwaptradeDetailComponent ],
      providers: [
        { provide: SwaptradesService,    useClass: SwapTradesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwaptradeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

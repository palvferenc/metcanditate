import { Component, OnInit, Input } from '@angular/core';
import { BooksService } from '../books.service';
import { Book } from '../models/book';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  @Input() newbook: Book;

  books: Book[];

  constructor(private bookService: BooksService) { }

  ngOnInit() {
    this.newbook = new Book();
    this.getBooks();
  }

  getBooks() : void
  {
    this.bookService.getBooks().subscribe(books => this.books = books);
  }

  add(): void {
    if(!this.newbook.name) { return; }
    this.bookService.addBook(this.newbook)
    .subscribe(user=>{this.books.push(user)})
    this.newbook = new Book();
  }

  delete(book: Book)
  {
    this.books = this.books.filter(c => c !== book );
    this.bookService.deleteBook(book).subscribe();
  }
}

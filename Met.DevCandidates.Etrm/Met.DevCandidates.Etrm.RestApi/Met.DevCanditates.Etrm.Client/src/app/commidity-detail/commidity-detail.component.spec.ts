import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommidityDetailComponent } from './commidity-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { CommiditiesService } from '../commidities.service';
import { CommoditiesServiceSpy } from '../../../testing/spies/commidites.service.spy';

describe('CommidityDetailComponent', () => {
  let component: CommidityDetailComponent;
  let fixture: ComponentFixture<CommidityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ CommidityDetailComponent ],
      providers: [
        { provide: CommiditiesService,    useClass: CommoditiesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommidityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { CommiditiesService } from '../commidities.service';
import { Commodity } from '../models/commodity';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-commidity-detail',
  templateUrl: './commidity-detail.component.html',
  styleUrls: ['./commidity-detail.component.css']
})
export class CommidityDetailComponent implements OnInit {

  @Input() commodity: Commodity;

  constructor(
    private route: ActivatedRoute,
    private commiditiesService: CommiditiesService,
    private location: Location
  ) {}

  ngOnInit() {
   this.getCommodity();
  }

  getCommodity(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.commiditiesService.getCommodity(id)
      .subscribe(commodity => this.commodity = commodity);
  }

  goBack(): void {
    this.location.back();
  }
 
  save(): void {
    this.commiditiesService.updateCommodity(this.commodity)
      .subscribe(() => this.goBack());
  }
}

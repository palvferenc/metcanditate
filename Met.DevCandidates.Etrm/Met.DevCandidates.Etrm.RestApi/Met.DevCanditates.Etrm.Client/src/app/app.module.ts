import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserService } from './user.service';
import { AppRoutingModule } from './app-routing.module';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { BooksComponent } from './books/books.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { CommiditiesComponent } from './commidities/commidities.component';
import { CommidityDetailComponent } from './commidity-detail/commidity-detail.component';
import { LegalentitiesComponent } from './legalentities/legalentities.component';
import { LegalentityDetailComponent } from './legalentity-detail/legalentity-detail.component';
import { SwaptradesComponent } from './swaptrades/swaptrades.component';
import { SwaptradeDetailComponent } from './swaptrade-detail/swaptrade-detail.component';
import { FuturestradesComponent } from './futurestrades/futurestrades.component';
import { FuturestradeDetailComponent } from './futurestrade-detail/futurestrade-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { BooksService } from './books.service';
import { LegalentitiesService } from './legalentities.service';
import { CommiditiesService } from './commidities.service';
import { FuturestradesService } from './futurestrades.service';
import { SwaptradesService } from './swaptrades.service';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserDetailComponent,
    BooksComponent,
    BookDetailComponent,
    CommiditiesComponent,
    CommidityDetailComponent,
    LegalentitiesComponent,
    LegalentityDetailComponent,
    SwaptradesComponent,
    SwaptradeDetailComponent,
    FuturestradesComponent,
    FuturestradeDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [UserService, 
    BooksService,
    LegalentitiesService, 
    CommiditiesService, 
    FuturestradesService,
    SwaptradesService],
  bootstrap: [AppComponent]
})
export class AppModule { }

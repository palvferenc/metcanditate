import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { BooksComponent } from './books/books.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { CommiditiesComponent } from './commidities/commidities.component';
import { CommidityDetailComponent } from './commidity-detail/commidity-detail.component';
import { LegalentitiesComponent } from './legalentities/legalentities.component';
import { LegalentityDetailComponent } from './legalentity-detail/legalentity-detail.component';
import { FuturestradesComponent } from './futurestrades/futurestrades.component';
import { FuturestradeDetailComponent } from './futurestrade-detail/futurestrade-detail.component';
import { SwaptradesComponent } from './swaptrades/swaptrades.component';
import { SwaptradeDetailComponent } from './swaptrade-detail/swaptrade-detail.component';

const routes: Routes = [
  {path: '', redirectTo: '/users',pathMatch: 'full'} ,
  {path: 'users', component: UsersComponent} ,
  {path: 'users/:id', component: UserDetailComponent} ,
  {path: 'books', component: BooksComponent} ,
  {path: 'books/:id', component: BookDetailComponent} ,
  {path: 'commidities', component: CommiditiesComponent} ,
  {path: 'commidities/:id', component: CommidityDetailComponent} ,
  {path: 'legalentities', component: LegalentitiesComponent} ,
  {path: 'legalentities/:id', component: LegalentityDetailComponent} ,
  {path: 'futurestrades', component: FuturestradesComponent} ,
  {path: 'futurestrades/:id', component: FuturestradeDetailComponent} ,
  {path: 'swaptrades', component: SwaptradesComponent} ,
  {path: 'swaptrades/:id', component: SwaptradeDetailComponent} ,
];

@NgModule({
  imports:[RouterModule.forRoot(routes)] ,
  exports:[RouterModule]
})
export class AppRoutingModule { }

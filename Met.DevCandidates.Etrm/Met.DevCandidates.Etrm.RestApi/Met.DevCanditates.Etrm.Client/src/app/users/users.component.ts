import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  @Input() newUser: User;

  users: User[];

  constructor(private userService : UserService) { }

  ngOnInit() {
    this.newUser= new User();
    this.getUsers();
  }

  getUsers() : void
  {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  add(): void {
    if(!this.newUser.firstName || !this.newUser.secondName) { return; }
    this.userService.addUser(this.newUser)
    .subscribe(user=>{this.users.push(user)})
    this.newUser= new User();
  }

  delete(user: User)
  {
    this.users = this.users.filter(u => u !== user );
    this.userService.deleteUser(user).subscribe();
  }
}

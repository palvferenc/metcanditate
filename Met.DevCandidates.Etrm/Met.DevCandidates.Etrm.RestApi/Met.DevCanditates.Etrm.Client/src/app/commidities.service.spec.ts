import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CommiditiesService } from './commidities.service';

describe('CommiditiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommiditiesService],
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ]
    });
  });

  it('should be created', inject([CommiditiesService], (service: CommiditiesService) => {
    expect(service).toBeTruthy();
  }));
});

import { Component, OnInit, Input } from '@angular/core';
import { FuturestradesService } from '../futurestrades.service';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FuturesTrade } from '../models/futurestrade';
import { UserService } from '../user.service';
import { BooksService } from '../books.service';
import { LegalentitiesService } from '../legalentities.service';
import { CommiditiesService } from '../commidities.service';
import { User } from '../models/user';
import { Book } from '../models/book';
import { LegalEntity } from '../models/legal-entity';
import { Commodity } from '../models/commodity';

@Component({
  selector: 'app-futurestrade-detail',
  templateUrl: './futurestrade-detail.component.html',
  styleUrls: ['./futurestrade-detail.component.css']
})
export class FuturestradeDetailComponent implements OnInit {

  @Input() futuresTrade: FuturesTrade;

  users: User[];
  books: Book[];

  entities: LegalEntity[];
  commodities: Commodity[];


  constructor(
    private route: ActivatedRoute,
    private futuresTradesService: FuturestradesService,
    private userService: UserService,
    private booksService: BooksService,
    private legalEntityService: LegalentitiesService,
    private commodityService:CommiditiesService,
    private location: Location
  ) {}

  ngOnInit() {
    this.getUsers();
    this.getBooks();
    this.getLegalEntities();
    this.getcCommodities();
    this.getFuturesTrade();
  }

  getFuturesTrade(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.futuresTradesService.getFuturesTrade(id)
      .subscribe(futuresTrade => this.futuresTrade = futuresTrade);
  }

  getUsers() : void
  {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  getcCommodities() : void
  {
    this.commodityService.getCommodities().subscribe(commidities => this.commodities = commidities);
  }

  getLegalEntities() : void
  {
    this.legalEntityService.getLegalEntities().subscribe(legalEntities => this.entities = legalEntities);
  }

  getBooks() : void
  {
    this.booksService.getBooks().subscribe(books => this.books = books);
  }

  goBack(): void {
    this.location.back();
  }
 
 save(): void {
  if(
    !this.futuresTrade.price 
    || !this.futuresTrade.deliveryDate 
    || !this.futuresTrade.traderId 
    || !this.futuresTrade.commodityId
    || !this.futuresTrade.counterPartyId
    || !this.futuresTrade.bookId
    || !this.futuresTrade.volume
    || !this.futuresTrade.tradeDate
  ) { return; }
    this.futuresTradesService.updateFuturesTrade(this.futuresTrade)
      .subscribe(() => this.goBack());
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuturestradeDetailComponent } from './futurestrade-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { FuturestradesService } from '../futurestrades.service';
import { FuturesTradesServiceSpy } from '../../../testing/spies/futurestrades.service.spy';

describe('FuturestradeDetailComponent', () => {
  let component: FuturestradeDetailComponent;
  let fixture: ComponentFixture<FuturestradeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ FuturestradeDetailComponent ],
      providers: [
        { provide: FuturestradesService,    useClass: FuturesTradesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuturestradeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

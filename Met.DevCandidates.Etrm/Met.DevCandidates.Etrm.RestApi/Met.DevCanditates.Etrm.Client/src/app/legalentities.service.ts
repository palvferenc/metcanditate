import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {LegalEntity} from './models/legal-entity'

const httpOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'})
};

@Injectable()
export class LegalentitiesService {

  private legalEntitiesUrl = 'api/legalentities';

  constructor( 
    private http: HttpClient
  ) { }

  getLegalEntities (): Observable<LegalEntity[]>
  {
    return this.http.get<LegalEntity[]>(this.legalEntitiesUrl)
    .pipe(
      catchError(this.handleError('getLegalEntities',[]))
    );
  }

  getLegalEntity(id: number): Observable<LegalEntity>{
    const url = `${this.legalEntitiesUrl}/${id}`;
    return this.http.get<LegalEntity>(url)
    .pipe(
      catchError(this.handleError<LegalEntity>('getLegalEntity id=${id}'))
    )
  }

  addLegalEntity(legalEntity: LegalEntity): Observable<LegalEntity>{
    return this.http.post<LegalEntity>(this.legalEntitiesUrl, legalEntity, httpOptions)
    .pipe(catchError(this.handleError<LegalEntity>('addLegalEntity')));
  }

  updateLegalEntity(legalEntity: LegalEntity): Observable<any>{
    const url = `${this.legalEntitiesUrl}/${legalEntity.id}`;
    return this.http.put(url, legalEntity, httpOptions)
    .pipe(
      catchError(this.handleError<any>('updateLegalEntity'))
    );
  }

  deleteLegalEntity (legalEntity: LegalEntity | number): Observable<LegalEntity> {
    const id = typeof legalEntity === 'number' ? legalEntity : legalEntity.id;
    const url = `${this.legalEntitiesUrl}/${id}`;
 
    return this.http.delete<LegalEntity>(url, httpOptions).pipe(
      catchError(this.handleError<LegalEntity>('deleteLegalEntity'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      
      console.error(error);
      
      return of(result as T);
    }
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {User} from './models/user'

const httpOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'})
};


@Injectable()
export class UserService {

  private usersUrl = 'api/users';

  constructor( 
    private http: HttpClient
  ) { }

  getUsers (): Observable<User[]>
  {
    return this.http.get<User[]>(this.usersUrl)
    .pipe(
      catchError(this.handleError('getUsers',[]))
    );
  }

  getUser(id: number): Observable<User>{
    const url = `${this.usersUrl}/${id}`;
    return this.http.get<User>(url)
    .pipe(
      catchError(this.handleError<User>('getUser id=${id}'))
    )
  }

  addUser(user: User): Observable<User>{
    return this.http.post<User>(this.usersUrl, user, httpOptions)
    .pipe(catchError(this.handleError<User>('addUser')));
  }

  updateUser(user: User): Observable<any>{
    const url = `${this.usersUrl}/${user.id}`;
    return this.http.put(url, user, httpOptions)
    .pipe(
      catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser (user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.usersUrl}/${id}`;
 
    return this.http.delete<User>(url, httpOptions).pipe(
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      
      console.error(error);
      
      return of(result as T);
    }
  }
}

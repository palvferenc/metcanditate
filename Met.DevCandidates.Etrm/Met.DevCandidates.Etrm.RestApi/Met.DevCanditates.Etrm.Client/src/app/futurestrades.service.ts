import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {FuturesTrade} from './models/futurestrade'

const httpOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'})
};

@Injectable()
export class FuturestradesService {

  private futureTradesUrl = 'api/futurestrades';

  constructor( 
    private http: HttpClient
  ) { }

  getFuturesTrades (): Observable<FuturesTrade[]>
  {
    return this.http.get<FuturesTrade[]>(this.futureTradesUrl)
    .pipe(
      catchError(this.handleError('getFuturesTrades',[]))
    );
  }

  getFuturesTrade(id: number): Observable<FuturesTrade>{
    const url = `${this.futureTradesUrl}/${id}`;
    return this.http.get<FuturesTrade>(url)
    .pipe(
      catchError(this.handleError<FuturesTrade>('getFuturesTrade id=${id}'))
    )
  }

  addFuturesTrade(SwapTrade: FuturesTrade): Observable<FuturesTrade>{
    return this.http.post<FuturesTrade>(this.futureTradesUrl, SwapTrade, httpOptions)
    .pipe(catchError(this.handleError<FuturesTrade>('addFuturesTrade')));
  }

  updateFuturesTrade(futuresTrade: FuturesTrade): Observable<any>{
    const url = `${this.futureTradesUrl}/${futuresTrade.id}`;
    return this.http.put(url, futuresTrade, httpOptions)
    .pipe(
      catchError(this.handleError<any>('updateFuturesTrade'))
    );
  }

  deleteFuturesTrade (futuresTrade: FuturesTrade | number): Observable<FuturesTrade> {
    const id = typeof futuresTrade === 'number' ? futuresTrade : futuresTrade.id;
    const url = `${this.futureTradesUrl}/${id}`;
 
    return this.http.delete<FuturesTrade>(url, httpOptions).pipe(
      catchError(this.handleError<FuturesTrade>('deleteFuturesTrade'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      
      console.error(error);
      
      return of(result as T);
    }
  }
}

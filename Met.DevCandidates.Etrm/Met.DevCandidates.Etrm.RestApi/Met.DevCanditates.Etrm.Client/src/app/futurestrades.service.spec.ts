import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { FuturestradesService } from './futurestrades.service';

describe('FuturestradesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FuturestradesService],
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ]
    });
  });

  it('should be created', inject([FuturestradesService], (service: FuturestradesService) => {
    expect(service).toBeTruthy();
  }));
});

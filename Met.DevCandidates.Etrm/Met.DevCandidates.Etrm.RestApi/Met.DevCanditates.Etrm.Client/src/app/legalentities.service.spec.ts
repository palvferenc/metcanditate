import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { LegalentitiesService } from './legalentities.service';

describe('LegalentitiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LegalentitiesService],
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ]
    });
  });

  it('should be created', inject([LegalentitiesService], (service: LegalentitiesService) => {
    expect(service).toBeTruthy();
  }));
});

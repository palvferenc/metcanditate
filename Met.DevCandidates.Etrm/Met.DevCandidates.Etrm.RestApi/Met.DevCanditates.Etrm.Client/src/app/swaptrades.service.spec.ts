import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SwaptradesService } from './swaptrades.service';

describe('SwaptradesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SwaptradesService],
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ]
    });
  });

  it('should be created', inject([SwaptradesService], (service: SwaptradesService) => {
    expect(service).toBeTruthy();
  }));
});

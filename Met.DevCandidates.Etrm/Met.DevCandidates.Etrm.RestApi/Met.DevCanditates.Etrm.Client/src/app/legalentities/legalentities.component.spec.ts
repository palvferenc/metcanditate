import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalentitiesComponent } from './legalentities.component';
import { RouterLinkStubDirective } from '../../../testing/router-stubs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { LegalentitiesService } from '../legalentities.service';
import { LegalEntitiesServiceSpy } from '../../../testing/spies/legalentities.service.spy';

describe('LegalentitiesComponent', () => {
  let component: LegalentitiesComponent;
  let fixture: ComponentFixture<LegalentitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ LegalentitiesComponent , RouterLinkStubDirective],
      providers: [
        { provide: LegalentitiesService,    useClass: LegalEntitiesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalentitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

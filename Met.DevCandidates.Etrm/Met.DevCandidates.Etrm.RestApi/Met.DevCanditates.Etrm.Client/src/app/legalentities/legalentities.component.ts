import { Component, OnInit, Input } from '@angular/core';
import { LegalentitiesService } from '../legalentities.service';
import { LegalEntity } from '../models/legal-entity';

@Component({
  selector: 'app-legalentities',
  templateUrl: './legalentities.component.html',
  styleUrls: ['./legalentities.component.css']
})
export class LegalentitiesComponent implements OnInit {

  @Input() newLegalEntity: LegalEntity;

  legalEntities: LegalEntity[];

  constructor(private legalEntitiesService: LegalentitiesService) { }

  ngOnInit() {
    this.newLegalEntity= new LegalEntity();
    this.getLegalEntitites();
  }

  getLegalEntitites() : void
  {
    this.legalEntitiesService.getLegalEntities().subscribe(legalEntities => this.legalEntities = legalEntities);
  }

  add(): void {
    if(!this.newLegalEntity.name) { return; }
    this.legalEntitiesService.addLegalEntity(this.newLegalEntity)
    .subscribe(user=>{this.legalEntities.push(user)})
    this.newLegalEntity= new LegalEntity();
  }

  delete(entity: LegalEntity)
  {
    this.legalEntities = this.legalEntities.filter(s => s !== entity );
    this.legalEntitiesService.deleteLegalEntity(entity).subscribe();
  }
}

import { Component, OnInit ,Input} from '@angular/core';
import { SwaptradesService } from '../swaptrades.service';
import { Commodity } from '../models/commodity';
import { SwapTrade } from '../models/swaptrade';
import { UserService } from '../user.service';
import { User } from '../models/user';
import { BooksService } from '../books.service';
import { LegalentitiesService } from '../legalentities.service';
import { CommiditiesService } from '../commidities.service';
import { LegalEntity } from '../models/legal-entity';
import { Book } from '../models/book';

@Component({
  selector: 'app-swaptrades',
  templateUrl: './swaptrades.component.html',
  styleUrls: ['./swaptrades.component.css']
})
export class SwaptradesComponent implements OnInit {

  @Input() newTrade: SwapTrade;

  users: User[];
  books: Book[];

  entities: LegalEntity[];
  commodities: Commodity[];

  swapTrades: SwapTrade[];

  constructor(
    private swapTradeService: SwaptradesService,
    private userService: UserService,
    private booksService: BooksService,
    private legalEntityService: LegalentitiesService,
    private commodityService:CommiditiesService
  ) { }

  ngOnInit() {
    this.newTrade= new SwapTrade();
    this.getUsers();
    this.getBooks();
    this.getLegalEntities();
    this.getcCommodities();
    this.getSwapTrades();
  }

  getSwapTrades() : void
  {
    this.swapTradeService.getSwapTrades().subscribe(swapTrades => this.swapTrades = swapTrades);
  }

  getUsers() : void
  {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  getcCommodities() : void
  {
    this.commodityService.getCommodities().subscribe(commidities => this.commodities = commidities);
  }

  getLegalEntities() : void
  {
    this.legalEntityService.getLegalEntities().subscribe(legalEntities => this.entities = legalEntities);
  }

  getBooks() : void
  {
    this.booksService.getBooks().subscribe(books => this.books = books);
  }

  add(): void {
    if(
      !this.newTrade.price 
      || !this.newTrade.pricingStart 
      || !this.newTrade.pricingEnd 
      || !this.newTrade.traderId 
      || !this.newTrade.commodityId
      || !this.newTrade.counterPartyId
      || !this.newTrade.bookId
      || !this.newTrade.volume
      || !this.newTrade.tradeDate
    ) { return; }
    this.swapTradeService.addSwapTrade(this.newTrade)
    .subscribe(swaptrade=>{this.swapTrades.push(swaptrade)})
    this.newTrade = new SwapTrade();
  }

  delete(swapTrade: SwapTrade)
  {
    this.swapTrades = this.swapTrades.filter(s => s !== swapTrade );
    this.swapTradeService.deleteSwapTrade(swapTrade).subscribe();
  }
}

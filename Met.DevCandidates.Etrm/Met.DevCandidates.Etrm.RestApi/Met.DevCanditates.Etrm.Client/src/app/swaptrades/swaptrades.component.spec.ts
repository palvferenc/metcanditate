import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwaptradesComponent } from './swaptrades.component';
import { RouterLinkStubDirective } from '../../../testing/router-stubs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { SwaptradesService } from '../swaptrades.service';
import { SwapTradesServiceSpy } from '../../../testing/spies/swaptrades.service.spy';

describe('SwaptradesComponent', () => {
  let component: SwaptradesComponent;
  let fixture: ComponentFixture<SwaptradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ SwaptradesComponent, RouterLinkStubDirective ],
      providers: [
        { provide: SwaptradesService,    useClass: SwapTradesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwaptradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

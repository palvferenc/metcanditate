import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalentityDetailComponent } from './legalentity-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { LegalentitiesService } from '../legalentities.service';
import { LegalEntitiesServiceSpy } from '../../../testing/spies/legalentities.service.spy';

describe('LegalentityDetailComponent', () => {
  let component: LegalentityDetailComponent;
  let fixture: ComponentFixture<LegalentityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ LegalentityDetailComponent ],
      providers: [
        { provide: LegalentitiesService,    useClass: LegalEntitiesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalentityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

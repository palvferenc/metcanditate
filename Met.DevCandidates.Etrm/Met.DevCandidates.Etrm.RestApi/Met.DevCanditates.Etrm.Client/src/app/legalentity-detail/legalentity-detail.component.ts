import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { LegalentitiesService } from '../legalentities.service';
import { LegalEntity } from '../models/legal-entity';

@Component({
  selector: 'app-legalentity-detail',
  templateUrl: './legalentity-detail.component.html',
  styleUrls: ['./legalentity-detail.component.css']
})
export class LegalentityDetailComponent implements OnInit {

  @Input() legalEntity: LegalEntity;

  constructor(
    private route: ActivatedRoute,
    private legalEntitiesService: LegalentitiesService,
    private location: Location
  ) {}

  ngOnInit() {
    this.getLegalEntity();
  }

  getLegalEntity(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.legalEntitiesService.getLegalEntity(id)
      .subscribe(swapTrade => this.legalEntity = swapTrade);
  }

  goBack(): void {
    this.location.back();
  }
 
 save(): void {
    this.legalEntitiesService.updateLegalEntity(this.legalEntity)
      .subscribe(() => this.goBack());
  }
}

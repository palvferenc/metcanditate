import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {SwapTrade} from './models/swaptrade'

const httpOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'})
};

@Injectable()
export class SwaptradesService {
  private swapTradesUrl = 'api/swaptrades';

  constructor( 
    private http: HttpClient
  ) { }

  getSwapTrades (): Observable<SwapTrade[]>
  {
    return this.http.get<SwapTrade[]>(this.swapTradesUrl)
    .pipe(
      catchError(this.handleError('getSwapTrades',[]))
    );
  }

  getSwapTrade(id: number): Observable<SwapTrade>{
    const url = `${this.swapTradesUrl}/${id}`;
    return this.http.get<SwapTrade>(url)
    .pipe(
      catchError(this.handleError<SwapTrade>('getSwapTrade id=${id}'))
    )
  }

  addSwapTrade(swapTrade: SwapTrade): Observable<SwapTrade>{
    return this.http.post<SwapTrade>(this.swapTradesUrl, swapTrade, httpOptions)
    .pipe(catchError(this.handleError<SwapTrade>('addSwapTrade')));
  }

  updateSwapTrade(swapTrade: SwapTrade): Observable<any>{
    const url = `${this.swapTradesUrl}/${swapTrade.id}`;
    return this.http.put(url, swapTrade, httpOptions)
    .pipe(
      catchError(this.handleError<any>('updateSwapTrade'))
    );
  }

  deleteSwapTrade (swapTrade: SwapTrade | number): Observable<SwapTrade> {
    const id = typeof swapTrade === 'number' ? swapTrade : swapTrade.id;
    const url = `${this.swapTradesUrl}/${id}`;
 
    return this.http.delete<SwapTrade>(url, httpOptions).pipe(
      catchError(this.handleError<SwapTrade>('deleteSwapTrade'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      
      console.error(error);
      
      return of(result as T);
    }
  }
}

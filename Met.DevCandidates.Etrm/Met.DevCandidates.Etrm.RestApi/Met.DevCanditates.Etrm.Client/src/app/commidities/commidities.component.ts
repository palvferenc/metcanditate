import { Component, OnInit, Input } from '@angular/core';
import { CommiditiesService } from '../commidities.service';
import { Commodity } from '../models/commodity';

@Component({
  selector: 'app-commidities',
  templateUrl: './commidities.component.html',
  styleUrls: ['./commidities.component.css']
})
export class CommiditiesComponent implements OnInit {

  @Input() newCommidity: Commodity;

  commodities: Commodity[];

  constructor(private commiditiesService: CommiditiesService) { }

  ngOnInit() {
    this.newCommidity = new Commodity();
    this.getCommidities();
  }

  getCommidities() : void
  {
    this.commiditiesService.getCommodities().subscribe(commodities => this.commodities = commodities);
  }

  add(): void {
    if(!this.newCommidity.name) { return; }
    this.commiditiesService.addCommodity(this.newCommidity)
    .subscribe(user=>{this.commodities.push(user)})
    this.newCommidity = new Commodity();
  }

  delete(commodity: Commodity)
  {
    this.commodities = this.commodities.filter(c => c !== commodity );
    this.commiditiesService.deleteCommodity(commodity).subscribe();
  }
}

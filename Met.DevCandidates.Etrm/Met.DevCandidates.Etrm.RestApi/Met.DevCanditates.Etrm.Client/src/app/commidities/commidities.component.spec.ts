import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommiditiesComponent } from './commidities.component';
import { RouterLinkStubDirective } from '../../../testing/router-stubs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { CommiditiesService } from '../commidities.service';
import { CommoditiesServiceSpy } from '../../../testing/spies/commidites.service.spy';

describe('CommiditiesComponent', () => {
  let component: CommiditiesComponent;
  let fixture: ComponentFixture<CommiditiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ CommiditiesComponent , RouterLinkStubDirective],
      providers: [
        { provide: CommiditiesService,    useClass: CommoditiesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommiditiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

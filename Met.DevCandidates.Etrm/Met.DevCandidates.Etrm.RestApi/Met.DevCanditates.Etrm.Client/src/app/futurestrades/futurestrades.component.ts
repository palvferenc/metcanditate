import { Component, OnInit, Input } from '@angular/core';
import { FuturestradesService } from '../futurestrades.service';
import { FuturesTrade } from '../models/futurestrade';
import { User } from '../models/user';
import { Book } from '../models/book';
import { LegalEntity } from '../models/legal-entity';
import { Commodity } from '../models/commodity';
import { UserService } from '../user.service';
import { BooksService } from '../books.service';
import { LegalentitiesService } from '../legalentities.service';
import { CommiditiesService } from '../commidities.service';

@Component({
  selector: 'app-futurestrades',
  templateUrl: './futurestrades.component.html',
  styleUrls: ['./futurestrades.component.css']
})
export class FuturestradesComponent implements OnInit {

  @Input() newTrade: FuturesTrade;

  users: User[];
  books: Book[];

  entities: LegalEntity[];
  commodities: Commodity[];

  futuresTrades: FuturesTrade[];

  constructor(private futuresTradesService: FuturestradesService,
    private userService: UserService,
    private booksService: BooksService,
    private legalEntityService: LegalentitiesService,
    private commodityService:CommiditiesService
  ) { }

  ngOnInit() {
    this.newTrade= new FuturesTrade();
    this.getUsers();
    this.getBooks();
    this.getLegalEntities();
    this.getcCommodities();
    this.getFuturesTrades();
  }

  getFuturesTrades() : void
  {
    this.futuresTradesService.getFuturesTrades().subscribe(futuresTrade => this.futuresTrades = futuresTrade);
  }

  getUsers() : void
  {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  getcCommodities() : void
  {
    this.commodityService.getCommodities().subscribe(commidities => this.commodities = commidities);
  }

  getLegalEntities() : void
  {
    this.legalEntityService.getLegalEntities().subscribe(legalEntities => this.entities = legalEntities);
  }

  getBooks() : void
  {
    this.booksService.getBooks().subscribe(books => this.books = books);
  }

  add(): void {
    if(
      !this.newTrade.price 
      || !this.newTrade.deliveryDate 
      || !this.newTrade.traderId 
      || !this.newTrade.commodityId
      || !this.newTrade.counterPartyId
      || !this.newTrade.bookId
      || !this.newTrade.volume
      || !this.newTrade.tradeDate
    ) { return; }
    this.futuresTradesService.addFuturesTrade(this.newTrade)
    .subscribe(futurestrade=>{this.futuresTrades.push(futurestrade)})
    this.newTrade= new FuturesTrade();
  }

  delete(futureTrade: FuturesTrade)
  {
    this.futuresTrades = this.futuresTrades.filter(s => s !== futureTrade );
    this.futuresTradesService.deleteFuturesTrade(futureTrade).subscribe();
  }
}

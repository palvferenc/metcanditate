import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuturestradesComponent } from './futurestrades.component';
import { RouterLinkStubDirective } from '../../../testing/router-stubs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { FuturestradesService } from '../futurestrades.service';
import { FuturesTradesServiceSpy } from '../../../testing/spies/futurestrades.service.spy';

describe('FuturestradesComponent', () => {
  let component: FuturestradesComponent;
  let fixture: ComponentFixture<FuturestradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ FuturestradesComponent, RouterLinkStubDirective ],
      providers: [
        { provide: FuturestradesService,    useClass: FuturesTradesServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuturestradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

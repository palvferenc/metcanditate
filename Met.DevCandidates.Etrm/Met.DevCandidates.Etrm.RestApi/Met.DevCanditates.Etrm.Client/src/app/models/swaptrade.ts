import {TradeBase} from './tradebase';


export class SwapTrade extends TradeBase {
    id: number;
    pricingStart: Date;
    pricingEnd:   Date;
}
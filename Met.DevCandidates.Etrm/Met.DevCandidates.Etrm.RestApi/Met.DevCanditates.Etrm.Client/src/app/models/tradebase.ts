import {Direction} from "./direction"
import { User } from "./user";
import { Book } from "./book";
import { Commodity } from "./commodity";

export class TradeBase{
    id: number;
    tradeDate: Date;
    traderId: number;
    trader: User;
    bookId: number;
    book: Book;
    counterPartyId: number;
    counterParty: User;
    price: number;
    volume: number;
    commodityId: number;
    commodity: Commodity;
    direction: Direction;
}
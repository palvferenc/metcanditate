import {TradeBase} from './tradebase';

export class FuturesTrade extends TradeBase{
    id: number;
    deliveryDate: Date;
}
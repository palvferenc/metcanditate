import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {Book} from './models/book'

const httpOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'})
};

@Injectable()
export class BooksService {

  private booksUrl = 'api/books';

  constructor( 
    private http: HttpClient
  ) { }

  getBooks (): Observable<Book[]>
  {
    return this.http.get<Book[]>(this.booksUrl)
    .pipe(
      catchError(this.handleError('getBooks',[]))
    );
  }

  getBook(id: number): Observable<Book>{
    const url = `${this.booksUrl}/${id}`;
    return this.http.get<Book>(url)
    .pipe(
      catchError(this.handleError<Book>('getBook id=${id}'))
    )
  }

  addBook(book: Book): Observable<Book>{
    return this.http.post<Book>(this.booksUrl, book, httpOptions)
    .pipe(catchError(this.handleError<Book>('addBook')));
  }

  updateBook(book: Book): Observable<any>{
    const url = `${this.booksUrl}/${book.id}`;
    return this.http.put(url, book, httpOptions)
    .pipe(
      catchError(this.handleError<any>('updateBook'))
    );
  }

  deleteBook (book: Book | number): Observable<Book> {
    const id = typeof book === 'number' ? book : book.id;
    const url = `${this.booksUrl}/${id}`;
 
    return this.http.delete<Book>(url, httpOptions).pipe(
      catchError(this.handleError<Book>('deleteSwapTrade'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      
      console.error(error);
      
      return of(result as T);
    }
  }
}

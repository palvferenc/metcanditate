import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {Commodity} from './models/commodity'

const httpOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'})
};

@Injectable()
export class CommiditiesService {

  private commoditiesUrl = 'api/commodities';

  constructor( 
    private http: HttpClient
  ) { }

  getCommodities (): Observable<Commodity[]>
  {
    return this.http.get<Commodity[]>(this.commoditiesUrl)
    .pipe(
      catchError(this.handleError('getCommodities',[]))
    );
  }

  getCommodity(id: number): Observable<Commodity>{
    const url = `${this.commoditiesUrl}/${id}`;
    return this.http.get<Commodity>(url)
    .pipe(
      catchError(this.handleError<Commodity>('getCommodity id=${id}'))
    )
  }

  addCommodity(commodity: Commodity): Observable<Commodity>{
    return this.http.post<Commodity>(this.commoditiesUrl, commodity, httpOptions)
    .pipe(catchError(this.handleError<Commodity>('addCommodity')));
  }

  updateCommodity(commodity: Commodity): Observable<any>{
    const url = `${this.commoditiesUrl}/${commodity.id}`;
    return this.http.put(url, commodity, httpOptions)
    .pipe(
      catchError(this.handleError<any>('updateCommodity'))
    );
  }

  deleteCommodity (commodity: Commodity | number): Observable<Commodity> {
    const id = typeof commodity === 'number' ? commodity : commodity.id;
    const url = `${this.commoditiesUrl}/${id}`;
 
    return this.http.delete<Commodity>(url, httpOptions).pipe(
      catchError(this.handleError<Commodity>('deleteCommodity'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      
      console.error(error);
      
      return of(result as T);
    }
  }
}

import { Directive, Input } from "@angular/core";

@Directive({
    selector: '[router-outlet]',
    host: {
      '(click)': 'onClick()'
    }
  })
  export class RouterOutletStubComponent {
    @Input('routerLink') linkParams: any;
    navigatedTo: any = null;
  
    onClick() {
      this.navigatedTo = this.linkParams;
    }
  }
import { User } from "../../src/app/models/user";

export class UserServiceSpy {

    USERS: User[] = [
        { id: 1, firstName: 'Mr. Nice' , secondName: 'adasdsa12213d'},
        { id: 2, firstName: 'Mr. asdsad' , secondName: 'adaaasad'},
        { id: 3, firstName: 'Mr. Nsds' , secondName: 'adassssad'},
      ];
      
    
    getUsers = jasmine.createSpy('getUsers').and.callFake(
    () => Promise
      .resolve(true)
      .then(() => Object.assign({}, this.USERS))
    );

    getUser = jasmine.createSpy('getUser').and.callFake(
      (id:number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.USERS[id]))
      );

    addUser = jasmine.createSpy('addUser').and.callFake(
      (user: User) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.USERS[0]))
      );

    updateUser = jasmine.createSpy('updateUser').and.callFake(
      (user: User) => Promise
        .resolve(true)
        .then(() => Object.assign(this.USERS[1], user))
      );

    deleteUser = jasmine.createSpy('deleteUser').and.callFake(
      (user: number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.USERS[0]))
      );
  }
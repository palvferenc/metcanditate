import { Book } from "../../src/app/models/book";

export class BooksServiceSpy {

    BOOKS: Book[] = [
        { id: 1, name: 'Book1' },
        { id: 2, name: 'Book2' },
        { id: 3, name: 'Book3' },
      ];
      
    
    getBooks = jasmine.createSpy('getBooks').and.callFake(
    () => Promise
      .resolve(true)
      .then(() => Object.assign({}, this.BOOKS))
    );

    getBook = jasmine.createSpy('getBook').and.callFake(
      (id:number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.BOOKS[id]))
      );

    addBook = jasmine.createSpy('addBook').and.callFake(
      (book: Book) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.BOOKS[0]))
      );

    updateBook = jasmine.createSpy('updateBook').and.callFake(
      (book: Book) => Promise
        .resolve(true)
        .then(() => Object.assign(this.BOOKS[1], book))
      );

    deleteBook = jasmine.createSpy('deleteBook').and.callFake(
      (book: number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.BOOKS[0]))
      );
  }
import { FuturesTrade } from "../../src/app/models/futurestrade";
import {Direction} from "../../src/app/models/direction"

export class FuturesTradesServiceSpy {

  FUTURES:FuturesTrade[] = [
    { id: 1, book:null, bookId : 1 ,commodity:null, commodityId: 1,counterParty: null, counterPartyId: 1, deliveryDate: new Date(2017,10,1), direction: Direction.Buy , price: 100, tradeDate: new Date(2017,10,2),trader: null,traderId: 2, volume: 10},
    { id: 2, book:null, bookId : 3 ,commodity:null, commodityId: 2,counterParty: null, counterPartyId: 2, deliveryDate: new Date(2017,10,3), direction: Direction.Sell, price: 125, tradeDate: new Date(2018,2,2), trader:null ,traderId: 1, volume: 100},
  ];
      
    
    getFuturesTrades = jasmine.createSpy('getFuturesTrades').and.callFake(
    () => Promise
      .resolve(true)
      .then(() => Object.assign({}, this.FUTURES))
    );

    getFuturesTrade = jasmine.createSpy('getFuturesTrade').and.callFake(
      (id:number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.FUTURES[id]))
      );

    addFuturesTrade = jasmine.createSpy('addFuturesTrade').and.callFake(
      (futuresTrade: FuturesTrade) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.FUTURES[0]))
      );

    updateFuturesTrade = jasmine.createSpy('updateFuturesTrade').and.callFake(
      (futuresTrade: FuturesTrade) => Promise
        .resolve(true)
        .then(() => Object.assign(this.FUTURES[1], futuresTrade))
      );

    deleteFuturesTrade = jasmine.createSpy('deleteFuturesTrade').and.callFake(
      (futuresTrade: number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.FUTURES[0]))
      );
  }
import { LegalEntity } from "../../src/app/models/legal-entity";

export class LegalEntitiesServiceSpy {

    LEGALS: LegalEntity[] = [
        { id: 1, name: 'Legal1' },
        { id: 2, name: 'Legal12' },
        { id: 3, name: 'Legal3' },
      ];
      
    
      getLegalEntities = jasmine.createSpy('getLegalEntities').and.callFake(
    () => Promise
      .resolve(true)
      .then(() => Object.assign({}, this.LEGALS))
    );

    getLegalEntity = jasmine.createSpy('getLegalEntity').and.callFake(
      (id:number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.LEGALS[id]))
      );

    addLegalEntity = jasmine.createSpy('addLegalEntity').and.callFake(
      (legalEntity: LegalEntity) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.LEGALS[0]))
      );

    updateLegalEntity = jasmine.createSpy('updateLegalEntity').and.callFake(
      (legalEntity: LegalEntity) => Promise
        .resolve(true)
        .then(() => Object.assign(this.LEGALS[1], legalEntity))
      );

    deleteLegalEntity = jasmine.createSpy('deleteLegalEntity').and.callFake(
      (legalEntity: number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.LEGALS[0]))
      );
  }
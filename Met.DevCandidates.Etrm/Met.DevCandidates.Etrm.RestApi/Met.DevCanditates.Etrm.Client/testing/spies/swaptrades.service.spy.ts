import { SwapTrade } from "../../src/app/models/SwapTrade";
import { Direction } from "../../src/app/models/direction";

export class SwapTradesServiceSpy {

    SWAPS: SwapTrade[] = [

      { id: 1, book:null, bookId : 1 ,commodity:null, commodityId: 1,counterParty: null, counterPartyId: 1, pricingStart: new Date(2017,10,1),pricingEnd: new Date(2017,10,2), direction: Direction.Buy , price: 100, tradeDate: new Date(2017,10,2),trader: null,traderId: 2, volume: 10},
      { id: 2, book:null, bookId : 3 ,commodity:null, commodityId: 2,counterParty: null, counterPartyId: 2, pricingStart: new Date(2017,10,1),pricingEnd: new Date(2017,10,2), direction: Direction.Sell, price: 125, tradeDate: new Date(2018,2,2), trader:null ,traderId: 1, volume: 100},
        ];
      
    
    getSwapTrades = jasmine.createSpy('getSwapTrades').and.callFake(
    () => Promise
      .resolve(true)
      .then(() => Object.assign({}, this.SWAPS))
    );

    getSwapTrade = jasmine.createSpy('getSwapTrade').and.callFake(
      (id:number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.SWAPS[id]))
      );

    addSwapTrade = jasmine.createSpy('addSwapTrade').and.callFake(
      (swapTrade: SwapTrade) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.SWAPS[0]))
      );

    updateSwapTrade = jasmine.createSpy('updateSwapTrade').and.callFake(
      (swapTrade: SwapTrade) => Promise
        .resolve(true)
        .then(() => Object.assign(this.SWAPS[1], swapTrade))
      );

    deleteSwapTrade = jasmine.createSpy('deleteSwapTrade').and.callFake(
      (swapTrade: number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.SWAPS[0]))
      );
  }
import { Commodity } from "../../src/app/models/commodity";

export class CommoditiesServiceSpy {

    COMMODITIES: Commodity[] = [
        { id: 1, name: 'Com1'},
        { id: 2, name: 'Com2'},
        { id: 3, name: 'Com3'},
      ];
      
    
    getCommodities = jasmine.createSpy('getCommodities').and.callFake(
    () => Promise
      .resolve(true)
      .then(() => Object.assign({}, this.COMMODITIES))
    );

    getCommodity = jasmine.createSpy('getCommodity').and.callFake(
      (id:number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.COMMODITIES[id]))
      );

    addCommodity = jasmine.createSpy('addCommodity').and.callFake(
      (commodity: Commodity) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.COMMODITIES[0]))
      );

    updateCommodity = jasmine.createSpy('updateCommodity').and.callFake(
      (commodity: Commodity) => Promise
        .resolve(true)
        .then(() => Object.assign(this.COMMODITIES[1], commodity))
      );

    deleteCommodity = jasmine.createSpy('deleteCommodity').and.callFake(
      (commodity: number) => Promise
        .resolve(true)
        .then(() => Object.assign({}, this.COMMODITIES[0]))
      );
  }
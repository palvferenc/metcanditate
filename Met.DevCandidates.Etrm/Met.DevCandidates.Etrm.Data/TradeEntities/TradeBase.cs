﻿using System;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;

namespace Met.DevCandidates.Etrm.Data.TradeEntities
{
    public abstract class TradeBase
    {
        public int Id { get; set; }
        public DateTime TradeDate { get; set; }

        public int TraderID { get; set; }
        public User Trader { get; set; }

        public int BookID { get; set; }
        public Book Book { get; set; }

        public int CounterpartyID { get; set; }
        public LegalEntity Counterparty { get; set; }

        public decimal Price { get; set; }
        public decimal Volume { get; set; }

        public int CommodityID { get; set; }
        public Commodity Commodity { get; set; }

        public Direction Direction { get; set; }

    }
}

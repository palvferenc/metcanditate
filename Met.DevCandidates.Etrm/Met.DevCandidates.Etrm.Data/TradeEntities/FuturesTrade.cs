﻿using System;

namespace Met.DevCandidates.Etrm.Data.TradeEntities
{
    public class FuturesTrade : TradeBase
    {
        public DateTime DeliveryDate { get; set; }
    }
}

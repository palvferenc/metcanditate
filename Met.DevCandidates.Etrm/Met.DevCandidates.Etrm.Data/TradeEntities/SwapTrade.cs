﻿using System;

namespace Met.DevCandidates.Etrm.Data.TradeEntities
{
    public class SwapTrade : TradeBase
    {
        public DateTime PricingStart { get; set; }
        public DateTime PricingEnd { get; set; }
    }
}

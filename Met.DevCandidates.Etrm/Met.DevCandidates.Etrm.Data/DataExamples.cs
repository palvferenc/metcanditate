﻿using System;
using System.Collections.Generic;
using System.Linq;
using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Data.TradeEntities;

namespace Met.DevCandidates.Etrm.Data
{
    public static class DataExamples
    {

        public static List<Book> GetAllBooks()
        {
            return new List<Book>
            {
                new Book{Name = "Physical Book"},
                new Book{Name = "Paper Book"},
                new Book{Name = "Foreign Exchange Book"}
            };
        }

        public static List<User> GetAllUsers()
        {
            return new List<User>
            {
                new User{FirstName = "Fred", SecondName = "Bloggs"},
                new User{FirstName = "Stephen", SecondName = "Malley"},
                new User{FirstName = "John", SecondName = "Smith"},
                new User{FirstName = "Neil", SecondName = "Barnet"}
            };
        }

        public static List<Commodity> GetAllCommodities()
        {
            return new List<Commodity>
            {
                new Commodity{Name = "Crude Oil"},
                new Commodity{Name = "Diesel"}
            };
        }

        public static List<LegalEntity> GetAlLegalEntities()
        {
            return new List<LegalEntity>
            {
                new LegalEntity{Name = "MOL"},
                new LegalEntity{Name = "Gazprom"}
            };
        }

        public static List<FuturesTrade> GetFuturesTrades()
        {
            var books = GetAllBooks();
            var users = GetAllUsers();
            var legalEntities = GetAlLegalEntities();
            var commodities = GetAllCommodities();

            return new List<FuturesTrade>
            {
                new FuturesTrade
                {
                    TradeDate = new DateTime(2018, 1, 2),
                    Price = 56.85M,
                    Volume = 1000,
                    DeliveryDate = new DateTime(2018, 12, 1),
                    Direction = Direction.Buy,
                    Trader = users.Single(user => user.FirstName == "John"),
                    Book = books.Single(book => book.Name == "Paper Book"),
                    Commodity = commodities.Single(commodity => commodity.Name == "Crude Oil"),
                    Counterparty = legalEntities.Single(legalEntity => legalEntity.Name == "Gazprom"),
                },
                new FuturesTrade
                {
                    TradeDate = new DateTime(2018, 1, 4),
                    Price = 56.01M,
                    Volume = 5000,
                    DeliveryDate = new DateTime(2018, 12, 1),
                    Direction = Direction.Sell,
                    Trader = users.Single(user => user.FirstName == "Fred"),
                    Book = books.Single(book => book.Name == "Paper Book"),
                    Commodity = commodities.Single(commodity => commodity.Name == "Diesel"),
                    Counterparty = legalEntities.Single(legalEntity => legalEntity.Name == "MOL"),
                }
            };
        }

        public static List<SwapTrade> GetSwapTrades()
        {
            var books = GetAllBooks();
            var users = GetAllUsers();
            var legalEntities = GetAlLegalEntities();
            var commodities = GetAllCommodities();
            return new List<SwapTrade>
            {
                new SwapTrade
                {
                    TradeDate = new DateTime(2018, 1, 2),
                    Price = 56.85M,
                    Volume = 1000,
                    PricingStart = new DateTime(2019, 1, 1),
                    PricingEnd = new DateTime(2019, 1, 31),
                    Direction = Direction.Buy,
                    Trader = users.Single(user => user.FirstName == "Stephen"),
                    Book = books.Single(book => book.Name == "Paper Book"),
                    Commodity = commodities.Single(commodity => commodity.Name == "Crude Oil"),
                    Counterparty = legalEntities.Single(legalEntity => legalEntity.Name == "Gazprom"),
                }
            };
        }

        public static List<TradeBase> GetAllTrades()
        {
            return GetFuturesTrades().Concat(GetSwapTrades().Cast<TradeBase>()).ToList();
        }

    }
}

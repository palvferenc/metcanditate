﻿using System;

namespace Met.DevCandidates.Etrm.Data.StaticDataEntities
{
    public class LegalEntity
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
}

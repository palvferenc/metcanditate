﻿using System;

namespace Met.DevCandidates.Etrm.Data.StaticDataEntities
{
    public class Book
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
}

﻿using System;

namespace Met.DevCandidates.Etrm.Data.StaticDataEntities
{
    public class User
    {
        public int Id { get; set; }
        public String FirstName { get; set; }
        public String SecondName { get; set; }
    }
}

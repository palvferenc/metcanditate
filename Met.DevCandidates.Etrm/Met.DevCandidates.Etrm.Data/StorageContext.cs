﻿using Met.DevCandidates.Etrm.Data.StaticDataEntities;
using Met.DevCandidates.Etrm.Data.TradeEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Met.DevCandidates.Etrm.Data
{
    public class StorageContext : DbContext
    {
        public StorageContext(){ }

        public StorageContext(DbContextOptions<StorageContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\metcandidatelocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(u => u.Id);
            modelBuilder.Entity<Book>().HasKey(b => b.Id);
            modelBuilder.Entity<Commodity>().HasKey(c => c.Id);
            modelBuilder.Entity<LegalEntity>().HasKey(l => l.Id);
            modelBuilder.Entity<FuturesTrade>().HasKey(f => f.Id);
            modelBuilder.Entity<SwapTrade>().HasKey(s => s.Id);

            modelBuilder.Entity<User>().Property(u => u.FirstName).IsRequired();
            modelBuilder.Entity<User>().Property(u => u.SecondName).IsRequired();
            
            modelBuilder.Entity<Book>().Property(b => b.Name).IsRequired();

            modelBuilder.Entity<Commodity>().Property(c => c.Name).IsRequired();

            modelBuilder.Entity<LegalEntity>().Property(l => l.Name).IsRequired();

            modelBuilder.Entity<FuturesTrade>().Property(f => f.DeliveryDate).IsRequired();
            modelBuilder.Entity<SwapTrade>().Property(s => s.PricingStart).IsRequired();
            modelBuilder.Entity<SwapTrade>().Property(s => s.PricingEnd).IsRequired();

            modelBuilder.Entity<TradeBase>().Property(t => t.Direction).IsRequired();
            modelBuilder.Entity<TradeBase>().Property(t => t.Price).IsRequired();
            modelBuilder.Entity<TradeBase>().Property(t => t.TradeDate).IsRequired();
            modelBuilder.Entity<TradeBase>().Property(t => t.Volume).IsRequired();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<LegalEntity> LegalEntities { get; set; }
        public DbSet<FuturesTrade> FuturesTrades { get; set; }
        public DbSet<SwapTrade> SwapTrades { get; set; }
    }
}

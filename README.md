# README #

Please follow the instuctions below to setup the required environemnt for the application

### .NET Web Api ###

* Please run database initialization, the applicating is using localdb, and sqlite for unit tests [EF Core Migration](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/)
* You can find several unit tests for services but please, only use UserService ones, others just skeletons...  

### Angular Application ###

* For developer environment, please use npm start, this will run ng serve with proxy configuration for web api
* Please use only firefox or chrome
* Swap trades, and Futures Trade detail page dropdowns are not bound initially, it is known bug
* Component tests are not finsihed, for swap and futures trades the spy services are not injected
